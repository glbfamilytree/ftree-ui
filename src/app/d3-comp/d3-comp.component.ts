import { Component, OnInit, ElementRef } from '@angular/core';
import { Http } from '@angular/http';
import * as d3 from 'd3';

@Component({
  selector: 'app-d3-comp',
  templateUrl: './d3-comp.component.html',
  styleUrls: ['./d3-comp.component.css']
})
export class D3CompComponent implements OnInit {
  host;
  svg;

  constructor(private element: ElementRef,private http : Http) {
    this.host = d3.select(this.element.nativeElement);
  }

  ngOnInit() {
     this.getData().subscribe( (data) => console.log(data.json()));
  }

  getData() {
    return this.http.get('assets/data.json');
  }
}
